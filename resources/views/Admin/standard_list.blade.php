

@extends('Admin.layouts.master')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>All Certified Products</h3>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->

                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title">


                                <a href="{{ url('admin/certified_product')}}" class="btn btn-block btn-success" onclick="">Add New product</a>


                            </h3>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped ">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Certificate Number</th>
                                    <th>Customer</th>
                                    <th>Product Standard</th>


                                    <th>Status </th>
                                    <th colspan="2">Action</th>


                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table_data as $key => $value)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $value->holder() }}</td>
                                        <td>{{ $value->customer_id }}</td>
                                        <td>{{ $value->standard }}</td>



                                        <td>{{ $value->status ? 'Active' :'Pending'}}</td>
                                        <td><a href="{{url('admin/form/'.$value->id)}}"><i class="fa fa-edit"></i></a> </td>
                                        <td><a class="btn btn-sm btn-outline-danger" onclick="shop_deactive({{$value->id}})"><i class="fa fa-trash" style="color: red"></i></a></td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>


@endsection
