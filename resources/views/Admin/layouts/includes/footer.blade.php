<footer class="main-footer">
    <strong>Copyright &copy; 2021-2022 <a href="https://www.ikramqa.com.my/">IKRAM QA SERVICES SDN</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.1.0-rc
    </div>
</footer>
