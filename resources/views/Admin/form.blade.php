<!-- import header -->
@extends('Admin.layouts.master')
<!-- import header -->

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        &nbsp;
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add Form</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">




                            @if($form == '')
                                @csrf
                                <form action="{{ url('admin/form') }}" method="post" enctype="multipart/form-data" >
                                    @else
                                        <form enctype="multipart/form-data" method="post" action="{{ url('admin/form/'.$form->id) }}">

                                            @csrf
                                            @method('PUT')


                                            @endif



                                            @include('Admin.layouts.errors')

                                            <div class="row">
                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label>Form Name</label>
                                                    <input type="text" class="form-control @error('name') is-invalid @enderror" required="" placeholder="Slider Name" name="name" value="{{ $form != ''? $form->name: old('name')}}">
                                                </div>

                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label>Form Code</label>
                                                    <input type="text" class="form-control @error('code') is-invalid @enderror" required="" placeholder="Slider Name" name="code" value="{{ $form != ''? $form->code: old('code')}}">
                                                </div>

                                                <div class="form-group  col-lg-6 col-md-6">
                                                    <label>Status</label>
                                                    <select class="form-control select2" name="status" style="width: 100%;">
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>

                                                <div class="form-group  col-lg-6 col-md-6">
                                                    <div class="form-group">
                                                        <input type="hidden" name="url" value="{{ $form != ''? $form->url: old('url')}}">
                                                        <input type="file" name="file"  placeholder="Choose file" id="file" value="{{ $form != ''? $form->url: old('url')}}">

                                                    </div>
                                                </div>


                                                <div class="form-group col-lg-6 col-md-6">
                                                    <label>Form Type</label>
                                                    <select name="type" class="form-control">
                                                        <option value="1" {{ ($form != '' && $form->type == "1")? "selected": "" }}>Product Certification</option>
                                                        <option value="2" {{ ($form != '' && $form->type == "2")? "selected": "" }}>Product Listing</option>
                                                        <option value="3" {{ ($form != '' && $form->type == "3")? "selected": "" }}>Testing Services</option>
                                                        <option value="4" {{ ($form != '' && $form->type == "4")? "selected": "" }}>Consignments</option>
                                                        <option value="4" {{ ($form != '' && $form->type == "5")? "selected": "" }}>Fees</option>
                                                    </select>
                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <label>Description</label>

                                                   <textarea type="text"  class="form-control textarea    " id="description" name="description" @if($form==null) value=""  @else value=" {{$form->description}}" @endif  ></textarea>

                                                  </div>






                                            <div class="form-group">
                                                <button type="submit" class="btn btn-lg btn-dark rounded px-5">Submit</button>
                                            </div>
                                        </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>

    <div id="uploadimageModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                        <div>
                            <div id="upload-demo"></div>
                            <input type="hidden" id="model_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="upload-image" class="btn btn-primary">Crop</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection
