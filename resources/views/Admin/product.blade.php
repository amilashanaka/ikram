<!-- import header -->
@extends('Admin.layouts.master')
<!-- import header -->

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        &nbsp;
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add Certified Product</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">


                            @if($form == null)

                                <form action="{{ route('certified_product.store') }}" method="post" enctype="multipart/form-data">
                                    @else
                                        <form enctype="multipart/form-data" method="post" action="{{ route('certified_product.update' ) }}">


                                            @endif

                                            @csrf

                                            @include('Admin.layouts.errors')

                                            <div class="row">
                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label>Name</label>
                                                    <input type="text" class="form-control @error('name') is-invalid @enderror" required="" placeholder="certified product Name" name="name"
                                                           value="{{ $form != ''? $form->name: old('name')}}">
                                                </div>

                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label>Certificate Number</label>
                                                    <input type="text" class="form-control @error('certificate_no') is-invalid @enderror" required="" placeholder="Certificate Number" name="certificate_no"
                                                           value="{{ $form != ''? $form->certificate_no: old('certificate_no')}}">
                                                </div>

                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label>Listing number</label>
                                                    <input type="text" class="form-control @error('listing_no') is-invalid @enderror" required="" placeholder="listing number" name="listing_no"
                                                           value="{{ $form != ''? $form->listing_no: old('listing_no')}}">
                                                </div>

                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label for="customer_id">Customer</label>
                                                    <select class="form-control" name="customer_id" id="customer_id">
                                                        @foreach($customer as $customer_select)
                                                            <option value="{{$customer_select->id}}">{{$customer_select->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>



                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label for="customer_id">Country Of Origin</label>
                                                    <select class="form-control" name="country" id="country">
                                                        @foreach($origin as $country_select)
                                                            <option value="{{$country_select->id}}">{{$country_select->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label for="customer_id">Standard</label>
                                                    <select class="form-control" name="standard" id="standard">
                                                        @foreach($standard as $standard_select)
                                                            <option value="{{$standard_select->id}}">{{$standard_select->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>




                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label>Expire Date</label>
                                                    <input type="date" class="form-control @error('expiry_date') is-invalid @enderror" required="" placeholder="Listing NUmber" name="expiry_date"
                                                           value="{{ $form != ''? $form->expiry_date: old('expiry_date')}}">
                                                </div>


                                                <div class="form-group     col-lg-6 col-md-6">
                                                    <label for="brand">Brands</label>
                                                    <select class="form-control" name="brand_id" id="brand_id">
                                                        @foreach($brand as $brand_select)
                                                            <option value="{{$brand_select->id}}">{{$brand_select->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>


                                                <div class="form-group  col-lg-6 col-md-6">
                                                    <label>Status</label>
                                                    <select class="form-control select2" name="status" style="width: 100%;">
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>


                                            </div>




                                            <div class="form-group">
                                                <label>Description</label>

                                                <textarea type="text" class="form-control textarea    " id="description" name="description" @if($form==null) value=""
                                                          @else value=" {{$form->description}}" @endif  ></textarea>

                                            </div>


                                            <div class="form-group">
                                                <button type="submit" class="btn btn-lg btn-dark rounded px-5">Submit</button>
                                            </div>
                                        </form>
                                </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>

    <div id="uploadimageModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                        <div>
                            <div id="upload-demo"></div>
                            <input type="hidden" id="model_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="upload-image" class="btn btn-primary">Crop</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection
