@extends('Home.layouts.master')

@section('content')

    <div class="page-header">
        <div class="container">
            <div class="breadc-box no-line">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="page-title">{{$product->name}}</h1>
                    </div>
                    <div class="col-md-6 mobile-left text-right">
                        <ul id="breadcrumbs" class="breadcrumbs none-style">
                            <li><a href="{{route('products')}}">Product Certification List</a></li>
                            <li class="active">detail of {{$product->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="entry-content">
        <div class="container">
            <div class="boxed-content">

                <div class="wpb_wrapper">

                    <table width="100%" height="100%" border="1" align="center" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td valign="top">
                                <script language="Javascript">
                                    jQuery(document).ready(function() {
                                        function fn_reloadlist(){
                                            parent.$("#example").dataTable().fnClearTable(0);
                                            parent.$("#example").dataTable().fnDraw();
                                        }

                                        function fn_closeframe(){
                                            fn_reloadlist();
                                            parent.$.modal.close();
                                        }

                                        $("#close").click(function(){
                                            fn_closeframe();
                                            return false;
                                        });

                                        $(".sp-load").hide();

                                        $("#print").click(function(){
                                            $.ajax({
                                                url: "/eMaterial/rpt/report",
                                                type : "POST",
                                                dataType : "json",
                                                data: {
                                                    ptype: $("#ptype").val(),
                                                    productID: $("#id").val(),
                                                    output: "F"
                                                },
                                                success: function(msg) {
                                                    location.href = "/eMaterial/" + msg.result;
                                                    return false;
                                                }
                                            });
                                        });
                                    });
                                </script>
                                <form name="myForm" id="myForm" method="post" action="">
                                    <input name="id" id="id" value="710" type="hidden">
                                    <input name="fn" id="fn" value="product" type="hidden">
                                    <input name="ftype" id="ftype" value="view" type="hidden">
                                    <input name="ptype" id="ptype" value="local" type="hidden">
                                    <div id="users-contain" class="ui-widget">

                                        <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding-top: 15px; text-align:center; font-weight: bold;"><h3>Detail's of Certificate of Conformity</h3></div>
                                        <table class="users-ui ui-widget ui-widget-content">
                                            <tbody><tr>
                                                <td width="20%" class="ui-widget-header">Certificate No</td>
                                                <td width="80%"> {{$product->certificate_no}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Expiry Date</td>
                                                <td>{{$product->expiry_date}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Name of Certificate Holder</td>
                                                <td>{{$product->customer->name}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Address of Certificate Holder</td>
                                                <td>{{$product->customer->address}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Listing No</td>
                                                <td>{{$product->customer->listing_no}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Status of Certificate Holder</td>
                                                <td><span class="bold">{{$product->customer->listing_no}}</span></td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Product Name</td>
                                                <td>{{$product->name}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Product Standard</td>
                                                <td>{{$product->std->name}}</td>
                                            </tr>
                                            <tr style="display:none;">
                                                <td class="ui-widget-header">Product Description</td>
                                                <td>Planted Steel Lighting Columns (Without Base Plant), Material: Steel, Material Grade: S275JR, Protection Treatment: Hot dip galvanizing. Height: Up to and including 10m, Type: Post Top, Shape: Octagonal, Door Reinforcement: Plain Bar Reinforced, Passive Safety: Class 0, Height: Up to and including 10m, Type: Post Top, Shape: Octagonal, Door Reinforcement: Deformed Bar Reinforced, Passive Safety: Class 0.</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Product Description</td>
                                                <td><div>{{strip_tags($product->description)}}</div></td>
                                            </tr>

                                            <tr>
                                                <td class="ui-widget-header">Brand</td>
                                                <td>(PLEASE REFER LOGO)</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Brand Logo</td>
                                                <td> <div><img src="{{$product->brand->logo}}" width="120"></div></td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Country of Origin</td>
                                                <td>{{$product->country}}</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Manufacturer's Name</td>
                                                <td>Segenting Bersatu Sdn. Bhd.</td>
                                            </tr>
                                            <tr>
                                                <td class="ui-widget-header">Address of Manufacturer</td>
                                                <td>Lot 5196, Kawasan Perindustrian Balakong Jaya, 43300 Seri Kembangan, Selangor.</td>
                                            </tr>
                                            </tbody></table>
                                        <table class="users-ui ui-widget ui-widget-content">
                                            <tbody><tr><td style="text-align:center;"><button name="print" id="print" type="button" onclick="window.print()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Print</span></button>&nbsp;<button name="close" id="close" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">Close</span></button></td></tr></tbody>
                                        </table>
                                    </div>
                                </form>

                            </td>
                        </tr>
                        </tbody></table>

                </div>


            </div>
        </div>
    </div>

@endsection
