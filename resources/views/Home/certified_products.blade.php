@extends('Home.layouts.master')

@section('content')

    <div class="page-header">
        <div class="container">
            <div class="breadc-box no-line">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="page-title">List of Certified Products </h1>
                    </div>
                    <div class="col-md-6 mobile-left text-right">
                        <ul id="breadcrumbs" class="breadcrumbs none-style">
                            <li><a href="{{route('home')}}">Home</a></li>
                            <li class="active">List of Certified Products</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row " style="text-align: center">

        <h2>List of Certified Products Sort by Name of Certificate Holder</h2>
    </div>


    <div class="row">

        <div class="col-2 col-md-2" >

            <div class="row" style="margin: 10px">

                <div class="col-12">

                    <div class="s5_module_box_1">
                        <div class="s5_module_box_2">
                            <div class="s5_mod_h3_outer">
                                <h3 class="s5_mod_h3"><span class="s5_h3_first">Sort </span> by</h3>
                            </div>

                            <ul class="menu">
                                <li class="item-259"><a href="/index.php/certholdname1">Name of Certificate Holder</a></li><li class="item-260"><a href="/index.php/productname1">Product Name</a></li><li class="item-261"><a href="/index.php/prostandard1">Product Standard</a></li><li class="item-327"><a href="/index.php/certnumb1">Certificate Number</a></li></ul>
                            <div style="clear:both; height:0px"></div>
                        </div>
                    </div>


                </div>


            </div>

            <div class="row" style="margin: 10px">

                <div class="col-12">


                    <div class="custom">
                        <p style="text-align: justify;">Note:</p>
                        <p style="text-align: justify;">1. Should there any dispute or discrete in the information published in this website, the actual issued certificate shall be the official document.</p>
                        <p style="text-align: justify;">2. Where the product's country of origin stated as 'MALAYSIA', the product is automatically listed in the 'Senarai Bahan Barangan / Binaan Tempatan' with similar information published this website.</p></div>


                </div>


            </div>

            <div class="boxed-content">

                <div class="module_round_box">







                </div>

            </div>


        </div>


        <div class="col-10 col-md-10">


            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
            <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>

            <section class="wpb_row row-fluid row-full-width row-no-padding table" style="padding: 20px">


                <table id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name of Certificate Holder</th>
                        <th>Product Name</th>
                        <th>Product Standard</th>
                        <th>Country of Origin</th>
                        <th>Expiry Date</th>
                        <th>Details</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($all as $key => $value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $value->customer->name}}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->std->name }}</td>
                            <td>{{ $value->origin->name}}</td>
                            <td>{{ $value->expiry_date }}</td>
                            <td><a href="{{url('certification/certified_product_detail/'.$value->id)}}"><i class="fa fa-eye"></i></a></td>

                        </tr>
                    @endforeach


                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name of Certificate Holder</th>
                        <th>Product Name</th>
                        <th>Product Standard</th>
                        <th>Country of Origin</th>
                        <th>Expiry Date</th>
                        <th>Details</th>
                    </tr>
                    </tfoot>
                </table>

            </section>


        </div>


    </div>



    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>




    <script type="text/javascript">
        $(() => {
            $('#example').DataTable();
        })
    </script>

@endsection

