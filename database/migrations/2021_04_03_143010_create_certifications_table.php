<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *    'name',
    'holder',
    'certificate_no',
    'stranded',
    'country_of_origin',
    'expire_date',
    'manufacture',
    'brand'
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certifications', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->nullable();
            $table->string('certificate_no');
            $table->string('country_of_origin')->nullable();
            $table->date('expire_date')->nullable();



            //fk


            $table->unsignedBigInteger('stranded_id')->nullable(true)->default(null)->index();
            $table->foreign('stranded_id')->references('id')->on('standards');

            $table->unsignedBigInteger('manufacture_id')->nullable(true)->default(null)->index();
            $table->foreign('manufacture_id')->references('id')->on('manufacturers');

            $table->unsignedBigInteger('brand_id')->nullable(true)->default(null)->index();
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certifications');
    }
}
