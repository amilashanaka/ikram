<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *      'name',
    'certificate_no',
    'expiry_date',
    'holder',
    'listing_no',
    'description',
    'brand',
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->nullable();
            $table->string('certificate_no');
            $table->date('expiry_date')->nullable();
            $table->string('listing_no')->nullable();
            $table->longText('description')->nullable();


            //fk


            $table->unsignedBigInteger('customer_id')->nullable(true)->default(null)->index();
            $table->foreign('customer_id')->references('id')->on('customers');

            $table->unsignedBigInteger('brand_id')->nullable(true)->default(null)->index();
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
