<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class FormController extends Controller {
    /**
     * @var \App\Models\Form
     */
    private $form;
    /**
     * @var \Illuminate\Support\Facades\Validator
     */
    private $validator;


    public function __construct() {
        $this->middleware('auth');
        $this->form      = new Form();
        $this->validator = new Validator();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        $form = null;

        return view('Admin.form', compact('form'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('Admin.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {


        $validate = Validator::make(
            $request->all(), [
                               'name' => 'required|string|max:255|unique:forms',
                               'code' => 'required',
                               'type' => 'required',
                               'file' => 'required|file|mimes:jpg,jpeg,bmp,png,doc,docx,csv,rtf,xlsx,xls,txt,pdf,zip',
                           ]
        );

        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput($request->all());
        }

        $fileName = time().'.'.$request->file->extension();

        $request->file->move(public_path('file'), $fileName);

        $request->merge(
            [
                'url' => '/file/'.$fileName,
            ]
        );


        try {

            $this->form->create($request->all());

            return redirect()->route('admin.form_list')->with('success', 'Form created successfully');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Something Went wrong please Try again..!');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Form $form
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form) {



        return view('Admin.form', compact('form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Form $form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form) {


        return view('Admin.form', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Form         $form
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form) {

        $validate = Validator::make(
            $request->all(), [
                               'name' => 'required|string|max:255|unique:forms',
                               'code' => 'required',
                               'type' => 'required',
                               'file' => 'required|file|mimes:jpg,jpeg,bmp,png,doc,docx,csv,rtf,xlsx,xls,txt,pdf,zip',
                           ]
        );

        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput($request->all());
        }

        $fileName = time().'.'.$request->file->extension();

        $request->file->move(public_path('file'), $fileName);

        $request->merge(
            [
                'url' => '/file/'.$fileName,
            ]
        );


        try {

            $form->update($request->all());

            return redirect()->route('admin.form_list')->with('success', 'Form created successfully');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Something Went wrong please Try again..!');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Form $form
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Form $form) {
        $form->delete();

        return redirect()->route('admin.form_list')->with('success', 'record deleted successfully');
    }

    public function form_list() {

        $table_data = Form::all();

        return view('Admin.form_list', compact('table_data'));


    }
}
