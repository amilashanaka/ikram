<?php

namespace App\Http\Controllers;


use App\Models\Brand;
use App\Models\Country;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Standard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller {


    /**
     * @var \Illuminate\Support\Facades\Validator
     */
    private $validator;
    private $product;


    public function __construct() {
        $this->middleware('auth');
        $this->product   = new Product();
        $this->validator = new Validator();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $form = null;
        $brand=Brand::all(['id','name']);
        $customer=Customer::all(['id','name']);
        $standard=Standard::all(['id','name']);
        $origin=Country::all(['id','name']);


        return view('Admin.product', compact('form','brand','customer','standard','origin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('Admin.product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {





        $validate = Validator::make(
            $request->all(), [
                               'name'           => 'required|string|max:255|unique:products',
                               'certificate_no' => 'required',
                               'listing_no'     => 'required'
                           ]
        );

        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput($request->all());
        }

        try {

            $this->product->create($request->all());

            return redirect()->route('admin.certified_product_list')->with('success', 'Form created successfully');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Something Went wrong please Try again..!');

        }


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) {

        return view('Admin.product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product) {
        return view('Admin.product', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product      $product
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Product $product) {
        $validate = Validator::make(
            $request->all(), [
                               'name'           => 'required|string|max:255|unique:products',
                               'certificate_no' => 'required',
                               'listing_no'     => 'required'
                           ]
        );

        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput($request->all());
        }

        try {

            $this->product->update($request->all());

            return redirect()->route('admin.product_list')->with('success', 'Form created successfully');

        } catch (Exception $e) {

            return redirect()->back()->with('error', 'Something Went wrong please Try again..!');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Product $product) {

        $product->delete();

        return redirect()->route('admin.product_list')->with('success', 'record deleted successfully');
    }

    public function certified_product_list() {

        $table_data = Product::all();

        return view('Admin.certified_product_list', compact('table_data'));
    }



}
