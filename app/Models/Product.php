<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    use HasFactory;

    protected $fillable = [
        'name',
        'certificate_no',
        'expiry_date',
        'customer_id',
        'status',
        'standard',
        'listing_no',
        'description',
        'brand_id',
        'country',
        'manufacture_id'

    ];


    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class,'brand_id');
    }

    public function std()
    {
        return $this->belongsTo(Standard::class,'standard');
    }

    public function origin(){

        return $this->belongsTo(Country::class,'country');

    }


    public function certificate()
    {
        return $this->hasMany(Certification::class);
    }

    public function manufacture(){

        return $this->belongsTo(Manufacturer::class,'manufacture_id');
    }
}
