<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Standard extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'specification',
        'code',
        'year',
        'description',
    ];

    public function certification()
    {
        return $this->hasMany(Certification::class);
    }
}
