<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'holder',
        'certificate_no',
        'stranded',
        'country_of_origin',
        'expire_date',
        'manufacture',
        'brand'
    ];

    public function holder()
    {
        return $this->belongsTo(Customer::class);
    }

    public function stranded()
    {
        return $this->belongsTo(Standard::class);
    }

    public function brand()
    {
        return $this->hasMany(Brand::class);
    }
}
